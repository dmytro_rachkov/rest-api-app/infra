Table of Contents
-----------------
1. [Description](#description)
2. [Pipeline](#pipeline)
3. [License](#license)
4. [Author](#author-information)

Description
=========

Repo provides a terraform code deploying EKS cluster and all accompanying resources required to serve the application. Kubernetes kubeconfig is saved to private remote s3 bucket.

Repo also illustrates development lifecycle with terraform that can be used in a team following strict gitops model, with peers review in MR and manual final deploy trigger once code has been merged to a master.


Pipeline
------------

A multistage gitlab-ci pipeline:
- validate stage:
  - init with testing of providers and setup of environment
  - validate will do syntax check
- Costs validation stage:
  -  [terraform-cost-estimation](https://github.com/antonbabenko/terraform-cost-estimation) tool was used to elaborate a budget/costs test where the monthly budget threshold is given as an input variable. The estimation is done with a simple condition to check if the planned deployment goint to exceed the threshold or not. [terraform.jq](https://github.com/antonbabenko/terraform-cost-estimation/blob/master/terraform.jq) is used  - all the credits for both solutions go to developer [Anton Babenko](https://github.com/antonbabenko)
  - as part of this job once test has successfully passed a json code MR comment (with costs estimation result) is going to be created via API of gitlab
- build stage:
  - will not only run plan and in-built in terraform code validation, but also is going to save it in .json format and cache it
  - is going to generate native terraform gitlab-ci report presenting it in MR
  - is going to make API call to gitlab server creating a discussions comment attaching pre-formatted plan using gitlab access token - that will ensure a peers during MR review can directly check in UI what has been created/changed/deleted in a plan.
- compliance stage:
  - utilizing Open Agent Policy conftest environment via [confectionery](https://github.com/Cigna/confectionery) project that provides a set of rules and policies over AWS terraform code by use of best practices. Hence, this stage enables static code testing capability to comply with these policies. All credits go to [confectionery](https://github.com/Cigna/confectionery)  and [conftest](https://www.conftest.dev/) communities.
- deploy: 
  - only when the MR is going to be merged to master this job is going to be activated giving a final manual step to press a button to actually deploy the resources (based on gitlab RBAC and master commits ruleset - this can be user with higher priviliges).


License
-------

GNU GPL license


Author Information
------------------

Dmitry Rachkov




