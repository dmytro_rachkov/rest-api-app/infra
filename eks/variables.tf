variable "region" {
  default = "us-east-1"
}

variable "cluster_name" {
  default = "docs-devops"
}

variable "map_accounts" {
  description = "Additional AWS account numbers to add to the aws-auth configmap."
  type        = list(string)

  default = [
    "871212273515"
  ]
}

variable "map_roles" {
  description = "Additional IAM roles to add to the aws-auth configmap."
  type = list(object({
    rolearn  = string
    username = string
    groups   = list(string)
  }))

  default = [
    {
      rolearn  = "arn:aws:iam::871212273515:role/eks_role1"
      username = "eks_role1"
      groups   = ["system:masters"]
    },
  ]
}

variable "map_users" {
  description = "Additional IAM users to add to the aws-auth configmap."
  type = list(object({
    userarn  = string
    username = string
    groups   = list(string)
  }))

  default = [
    {
      userarn  = "arn:aws:iam::871212273515:user/mrbot"
      username = "mrbot"
      groups   = ["system:masters"]
    }
  ]
}

variable "s3_bucket" {
}