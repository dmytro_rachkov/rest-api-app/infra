terraform {
  backend "s3" {
    bucket                      = "docs-devops-tfstate"
    region                      = "us-east-1"
    force_path_style            = true
    key                         = "terraform/r1/eks-docsdevops.tfstate"
    profile                     = "default"
  }
}
